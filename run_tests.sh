#!/bin/bash

mkdir -p out_basic
cd out_basic

for filename in ../public_basic_tests/input/*.json; do
	python3 ../bo-analyser.py "$filename"
done

cd ..
mkdir -p out_advanced
cd out_advanced

for filename in ../public_advanced_tests/input/*.json; do
        python3 ../bo-analyser.py "$filename"
done
