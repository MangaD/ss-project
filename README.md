# SS Project

**Course:** Software Security  
**University:** Instituto Superior Técnico  
**Academic year:** 2018-19

### Team

73891 - David Gonçalves [david.s.goncalves@tecnico.ulisboa.pt](mailto:david.s.goncalves@tecnico.ulisboa.pt)  
76470 - Francisco Correia [francisco.g.correia@tecnico.ulisboa.pt](mailto:francisco.g.correia@tecnico.ulisboa.pt)  
76494 - Guilherme Menezes [guilherme.menezes@tecnico.ulisboa.pt](mailto:guilherme.menezes@tecnico.ulisboa.pt)

## Assignment

See `documentation/Assignment.md`

## Methodology

See `documentation/Methodology.md`

## Running the tool

Our tool is written in **Python 3**. To analyse a file simply run:

```sh
python3 bo-analyser <program>.json
```

Where `<program>.json` is a JSON file with the format specified in the assignment. A file `<program>.output.json` will be generated reporting the vulnerabilities found.

# Testing

Run the tool for all input files:

```sh
./run_tests.sh
```

## To do

1. Map content of local variables to know where `\0` appears.
2. If buffer is uninitialized or not null terminated, set its content length until `\0` if we know it or indefinitely if we don't know it.
3. Consider static variables (RIP referenced).
4. Assembly instruction `test`.
5. Consider parameters in the stack (after 6th parameter).
6. Parameters can be literals.
7. File name path may not have `./` as prefix (bug).
8. Functions like `scanf` can receive more arguments and of different types.